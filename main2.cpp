#include "aicube.h"

#include <limits.h>
#include <time.h>


int nMinDepth = -1;

extern int nCnt;


void Game(Cube cb, int nF, bool bTurn, int nDepth, int nMaxDepth)
{

    if(nDepth > nMaxDepth)
        return;
    ++nCnt;

    vector<int*> vF= cb.GetFace(nF);
    vector<int> v = vTurns(nF);

    for(int y = 0; y < 3; ++y)
    for(int x = 0; x < 3; ++x)
    {
        int* pI = AtFace(vF, x, y);
        if(*pI != 0)
            continue;
        *pI = bTurn ? 1 : 2;
        if(cb.CheckWin(1) || cb.CheckWin(2))
            return;
        *pI = 0;
    }

    for(int y = 0; y < 3; ++y)
    for(int x = 0; x < 3; ++x)
    {
        int* pI = AtFace(vF, x, y);
        if(*pI != 0)
            continue;
        *pI = bTurn ? 1 : 2;
        for(int i = 0; i < 4; ++i)
        {
            Game(cb, v.at(i), !bTurn, nDepth + 1, nMaxDepth);
        }

        *pI = 0;
    }

    if(nMinDepth == -1 || nDepth < nMinDepth)
    {
        nMinDepth = nDepth;
        cout << nMinDepth << "\n";
    }
}

int main2()
{

    /*
    vector<int> itr(3, 0);
    for(itr[2] = 0; itr[2] < 3; ++itr[2])
    for(itr[1] = 0; itr[1] < 3; ++itr[1])
    for(itr[0] = 0; itr[0] < 3; ++itr[0])
    {
        Cube cb;
        *cb.at(itr) = 1;
         cb.Out(cout);
         cin.get();
    }
    */

    /*
    Cube cb;

    for(;;)
    {
        int x, y, z, n;
        cin >> x >> y >> z >> n;
        *cb.at(x, y, z) = n;
        cb.Out(cout);
        if(cb.CheckWin(1))
            std::cout << "First wins\n";
        if(cb.CheckWin(2))
            std::cout << "Second wins\n";
    }

    for(int i = 0; i < 6; ++i)
    {
        vector<int> v = vTurns(i);
        cout << i << " : ";
        for(unsigned j = 0; j < v.size(); ++j)
            cout << v[j] << " ";
        cout << "\n";
    }
    */

    int n = clock();

    Game(Cube(), 0, true, 0, 3);

    cout << clock() - n << " ms\n";
    cout << nCnt << " calls\n";

    return 0;
}
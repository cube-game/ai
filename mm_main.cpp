#include <iostream>
#include <vector>
#include <time.h>

#include "CubeMaxMin.h"

using namespace std;
using namespace AntonAi;

int main()
{
    srand( unsigned (time(0)) );

	int nPlayer = PLAYER_MAX;
	int nMaxSize = 4;

	while(true)
	{
		Game gm(4, nPlayer);

		while(true)
		{
			PrintGame(&gm);
			
			if(gm.CheckForWin() != VL_UNDEF)
			{
				cout << "Game won\n";
				std::cin.get();
				break;
			}

			IterativeStrategy itst(gm, AD_HARD);

			for(int i = 0; i < 1000000; ++i)
			{
				if(itst.Step<true>())
				{
					std::cout << "Step: " << i << " ";
					std::cout << "Depth: " << itst.nCurrDepth - 1
						<< " val " << itst.vResults.back().GetBestValue();
						//<< " mp ";// << itst.vResults.back().mMoves.size()
					
					//itst.vResults.back().PrintMap();
					
					std::cout << "\n";

					if(itst.bDoneAnalysis)
						break;
				}
			}


			//std::cout << "Moves: " << itst.vResults.back().vBestMoves.size() << "\n\n";

			gm.ApplyMove(itst.vResults.back().GetMove(), gm);
		}
	}
	
	return 0;
}
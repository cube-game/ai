#include "aicube.h"

struct Turn
{
    Cube cb;
    int nF;
    bool bTurn;
    int nDepth;
};

void Game(Turn& t, list<Turn>& ls)
{    
    vector<int*> vF= t.cb.GetFace(t.nF);
    vector<int> v = vTurns(t.nF);

    for(int y = 0; y < 3; ++y)
    for(int x = 0; x < 3; ++x)
    {
        int* pI = AtFace(vF, x, y);
        if(*pI != 0)
            continue;
        *pI = t.bTurn ? 1 : 2;
        if(t.cb.CheckWin(1) || t.cb.CheckWin(2))
            return;
        *pI = 0;
    }

    for(int y = 0; y < 3; ++y)
    for(int x = 0; x < 3; ++x)
    {
        int* pI = AtFace(vF, x, y);
        if(*pI != 0)
            continue;
        *pI = t.bTurn ? 1 : 2;
        for(int i = 0; i < 4; ++i)
        {
            Turn new_t;
            new_t.cb = t.cb;
            new_t.bTurn = !t.bTurn;
            new_t.nF = v.at(i);
            new_t.nDepth = t.nDepth + 1;
            
            ls.push_back(new_t);
        }

        *pI = 0;
    }
}

int main_alt()
{

    /*
    vector<int> itr(3, 0);
    for(itr[2] = 0; itr[2] < 3; ++itr[2])
    for(itr[1] = 0; itr[1] < 3; ++itr[1])
    for(itr[0] = 0; itr[0] < 3; ++itr[0])
    {
        Cube cb;
        *cb.at(itr) = 1;
         cb.Out(cout);
         cin.get();
    }
    */

    /*
    Cube cb;

    for(;;)
    {
        int x, y, z, n;
        cin >> x >> y >> z >> n;
        *cb.at(x, y, z) = n;
        cb.Out(cout);
        if(cb.CheckWin(1))
            std::cout << "First wins\n";
        if(cb.CheckWin(2))
            std::cout << "Second wins\n";
    }

    for(int i = 0; i < 6; ++i)
    {
        vector<int> v = vTurns(i);
        cout << i << " : ";
        for(unsigned j = 0; j < v.size(); ++j)
            cout << v[j] << " ";
        cout << "\n";
    }
    */

    list<Turn> ls;
    
    Turn t;
    t.cb = Cube();
    t.bTurn = true;
    t.nF = 0;
    t.nDepth = 0;

    ls.push_back(t);

    int nDepth = 0;
    int nSize = 0;
    
    while(ls.size())
    {
        Turn t = ls.front();
        ls.pop_front();

        if(t.nDepth != nDepth)
        {
            nDepth = t.nDepth;
            cout << "\nDepth: " << nDepth << "\n";
        }

        if(ls.size() > nSize + 10000)
        {
            nSize = ls.size();
            cout << "Size: " << nSize << "\r";
        }

        Game(t, ls);
    }

    return 0;
}

/*

struct Pair
{
    vector<int> v;

    Pair(int n1, int n2)
    {
        v.push_back(n1);
        v.push_back(n2);

        sort(v.begin(), v.end());
    }

    bool operator == (const Pair& p) const {return v == p.v;}
    bool operator < (const Pair& p) const {return v < p.v;}
};

void AnalyzeTriple(set<Pair>& sFill, int n1, int n2, int n3, int nNum)
{
    if(n1 == nNum)
        sFill.insert(Pair(n2, n3));
    else if(n2 == nNum)
        sFill.insert(Pair(n1, n3));
    else if(n3 == nNum)
        sFill.insert(Pair(n1, n2));
}

#define CMB(A, B, C) (AnalyzeTriple(vResult[i], face[j * 9 + A], face[j * 9 + B], face[j * 9 + C], i))

void GenerateCombos()
{
    int face[9*6] =
                    {18, 9, 0, 21, 12, 3, 24, 15, 6,
                     2, 11, 20, 5, 14, 23, 8, 17, 26,
                     18, 19, 20, 9, 10, 11, 0, 1, 2,
                     6, 7, 8, 15, 16, 17, 24, 25, 26,
                     0, 1, 2, 3, 4, 5, 6, 7, 8,
                     24, 25, 26, 21, 22, 23, 18, 19, 20};

    vector< set<Pair> > vResult(27);
    
    for(int i = 0; i < 27; ++i)
    {
        for(int j = 0; j < 6; ++j)
        {
            CMB(0, 1, 2);
            CMB(3, 4, 5);
            CMB(6, 7, 8);

            CMB(0, 3, 6);
            CMB(1, 4, 7);
            CMB(2, 5, 8);

            CMB(0, 4, 8);
            CMB(2, 4, 6);
        }

        set<Pair>& st = vResult[i];
        cout << st.size() << ", ";
        for(set<Pair>::iterator itr = st.begin(), etr = st.end(); itr != etr; ++itr)
            cout << itr->v[0] << ", " << itr->v[1] << ", ";
        for(int n = 0; n < (6 - st.size()); ++n)
            cout << "0, 0, ";
        cout << "\n";
    }

}


*/
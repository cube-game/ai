# The name of our project is "AICUBE". CMakeLists files in this project can 
# refer to the root source directory of the project as ${AICUBE_SOURCE_DIR} and 
# to the root binary directory of the project as ${AICUBE_BINARY_DIR}. 
cmake_minimum_required (VERSION 2.4) 
project (AICUBE)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)

add_executable(AICUBE main.cpp main2.cpp main_alt.cpp aicube.cpp aicube.h recursive_main.cpp)

# The name of our project is "MMCUBE". CMakeLists files in this project can 
# refer to the root source directory of the project as ${MMCUBE_SOURCE_DIR} and 
# to the root binary directory of the project as ${MMCUBE_BINARY_DIR}. 
cmake_minimum_required (VERSION 2.4) 
project (MMCUBE)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)

add_executable(MMCUBE CubeMaxMin.h CubeMaxMin.cpp MaxMinAi.h MaxMinAi.cpp mm_main.cpp)

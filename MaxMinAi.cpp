#include "MaxMinAi.h"
#include <iostream>

namespace AntonAi
{
	void PrintGame(Game* pGm)
	{
		std::cout << "Player: " << pGm->nPlayer << " Side: " << pGm->nCurrSide << "\n";
		PrintCube(pGm->arrCube);
		std::cout << "\n";
	}

	void IterativeStrategy::InitializeNewGame()
	{
		vNodes.clear();
		
		vNodes.push_back(Node(gm.nPlayer));
		
		Node& nd = vNodes.back();

		nd.gm = gm;

		nd.MvCurrent.InitializeFromGame(nd.gm);
	}

	IterativeStrategy::IterativeStrategy(Game gm_, AiDifficulty ad_)
		:gm(gm_), nCurrDepth(2), bDoneAnalysis(false), nMaxDepth(100), ad(ad_), st(gm_.nPlayer, ad_)
	{
		if(ad == AD_EASY)
			nMaxDepth = 2;
		
		if(ad == AD_EASY)
			std::cout << "Easy mode initialized\n";
		else if(ad == AD_MEDIUM)
			std::cout << "Medium mode initialized\n";
		else if(ad == AD_HARD)
			std::cout << "Hard mode initialized\n";
		else
			std::cout << "Unknown mode initialized!\n";
		
		vNodes.reserve(100);
		InitializeNewGame();
	}

	Move IterativeStrategy::GetMove()
	{
#ifdef AI_DEBUG_MODE		
		if(vResults.empty())
			throw int (237);
#endif
		//if(vResults.back().vBestMoves.empty())
		//	throw int (238);
		
		//Statistics st = vResults.back();

		//Move mv = st.vBestMoves[rand()%st.vBestMoves.size()];
		Move mv = vResults.back().GetMove();

		mv.nTurnSide = AntonAi::GetMove(gm.nCurrSide, mv.nTurnSide);
		
		PrintGame(&gm);
		std::cout << "\n";
		std::cout << "Best value: " << vResults.back().GetBestValue() << "\n";
		std::cout << "Move " << mv.nMoveSquare << " " << mv.nTurnSide << "\n";
		
		return mv;
	}
}
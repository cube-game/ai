#pragma once 

#include <iostream>
#include <vector>
#include <list>
#include <string>

using namespace std;

struct Cube
{
    vector<int> vEls;

    int* at(vector<int> crd);
    int* at(int x, int y, int z);

    Cube():vEls(27, 0){}

    vector<int*> GetFace(int nF);
    vector<int*> GetFace(vector<int> crd);

    void Out(std::ostream& ostr);

    bool CheckWin(int n);
};

int* AtFace(vector<int*> vF, int x, int y);

vector<int*> FlipH(vector<int*> vF);
vector<int*> FlipV(vector<int*> vF);
vector<int*> Transpose(vector<int*> vF);

bool CheckWin(vector<int*> vF, int n);

int Opp(int nF);

vector<int> vTurns(int nF);
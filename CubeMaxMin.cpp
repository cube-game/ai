#include "CubeMaxMin.h"

namespace AntonAi
{

	typedef signed char aibyte;

	using namespace std;

	int face[9*6] =
					{18, 9, 0, 21, 12, 3, 24, 15, 6,
					 2, 11, 20, 5, 14, 23, 8, 17, 26,
					 18, 19, 20, 9, 10, 11, 0, 1, 2,
					 6, 7, 8, 15, 16, 17, 24, 25, 26,
					 0, 1, 2, 3, 4, 5, 6, 7, 8,
					 24, 25, 26, 21, 22, 23, 18, 19, 20};

	inline aibyte& GetFaceAt(aibyte* arrCube, int nFace, int nPos)
	{
		return arrCube[face[nFace * 9 + nPos]];
	}

	inline bool WinCheck(aibyte* arrCube, int nFace, int nMove, int n)
	{
		int nCombos[ (6*2 + 1) * 27] = {
			6,  1, 2, 3, 6, 4, 8, 9, 18, 10, 20, 12, 24,
			3,  0, 2, 4, 7, 10, 19, 0, 0, 0, 0, 0, 0,
			6,  0, 1, 4, 6, 5, 8, 10, 18, 11, 20, 14, 26,
			3,  0, 6, 4, 5, 12, 21, 0, 0, 0, 0, 0, 0,
			4,  0, 8, 1, 7, 2, 6, 3, 5, 0, 0, 0, 0,
			3,  2, 8, 3, 4, 14, 23, 0, 0, 0, 0, 0, 0,
			6,  0, 3, 2, 4, 7, 8, 12, 18, 15, 24, 16, 26,
			3,  1, 4, 6, 8, 16, 25, 0, 0, 0, 0, 0, 0,
			6,  0, 4, 2, 5, 6, 7, 14, 20, 16, 24, 17, 26,
			3,  0, 18, 10, 11, 12, 15, 0, 0, 0, 0, 0, 0,
			4,  0, 20, 1, 19, 2, 18, 9, 11, 0, 0, 0, 0,
			3,  2, 20, 9, 10, 14, 17, 0, 0, 0, 0, 0, 0,
			4,  0, 24, 3, 21, 6, 18, 9, 15, 0, 0, 0, 0,
			0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			4,  2, 26, 5, 23, 8, 20, 11, 17, 0, 0, 0, 0,
			3,  6, 24, 9, 12, 16, 17, 0, 0, 0, 0, 0, 0,
			4,  6, 26, 7, 25, 8, 24, 15, 17, 0, 0, 0, 0,
			3,  8, 26, 11, 14, 15, 16, 0, 0, 0, 0, 0, 0,
			6,  0, 9, 2, 10, 6, 12, 19, 20, 21, 24, 22, 26,
			3,  1, 10, 18, 20, 22, 25, 0, 0, 0, 0, 0, 0,
			6,  0, 10, 2, 11, 8, 14, 18, 19, 22, 24, 23, 26,
			3,  3, 12, 18, 24, 22, 23, 0, 0, 0, 0, 0, 0,
			4,  18, 26, 19, 25, 20, 24, 21, 23, 0, 0, 0, 0,
			3,  5, 14, 20, 26, 21, 22, 0, 0, 0, 0, 0, 0,
			6,  0, 12, 6, 15, 8, 16, 18, 21, 20, 22, 25, 26,
			3,  7, 16, 19, 22, 24, 26, 0, 0, 0, 0, 0, 0,
			6,  2, 14, 6, 16, 8, 17, 18, 22, 20, 23, 24, 25
		};

		int nOff = face[nFace * 9 + nMove] * (6*2 + 1) + 1;
		int sz = nCombos[nOff - 1];
		for(int i = 0; i < sz; ++i)
			if( (arrCube[nCombos[nOff + i*2]] == n) && (arrCube[nCombos[nOff + i*2 + 1]] == n))
				return true;
		return false;
	}

	int FaceControl(aibyte* arrCube, int nFace, int nPlayer)
	{
		int nRet = 0;
		for(int i = 0; i < 9; ++i)
		{
		   if(GetFaceAt(arrCube, nFace, i) != 0)
				continue;
			if(WinCheck(arrCube, nFace, i, nPlayer))
			{
				if(nRet == 0)
					nRet += 10;
				else
					nRet += 5;
				if(i == 4)
					nRet += 5;
			}
		}
		return nRet;
	}

	int GetHeuristic(aibyte* arrCube, int nPlayer)
	{
		int nRet = 0;
	    
		int nCorners[8] = {0, 2, 6, 8, 18, 20, 24, 26};
		int i;
	    
		for(i = 0; i < 8; ++i)
			if(arrCube[nCorners[i]] == nPlayer)
				++nRet;
		for(i = 0; i < 6; ++i)
			nRet += FaceControl(arrCube, i, nPlayer);

		nRet *= nPlayer;

		return nRet;
	}

	bool Triple(aibyte* arrCube, int nFace, int nPlayer, int a, int b, int c)
	{
		return 
			(GetFaceAt(arrCube, nFace, a) == nPlayer) &&
			(GetFaceAt(arrCube, nFace, b) == nPlayer) &&
			(GetFaceAt(arrCube, nFace, c) == nPlayer);
	}

	int WinCheck(aibyte* arrCube)
	{
		for(int n = 1; n <= 2; ++n)
		for(int f = 0; f < 6; ++f)
		{
			if(
				Triple(arrCube, f, n, 0, 1, 2) ||
				Triple(arrCube, f, n, 3, 4, 5) ||
				Triple(arrCube, f, n, 6, 7, 8) ||
				Triple(arrCube, f, n, 0, 3, 6) ||
				Triple(arrCube, f, n, 1, 4, 7) ||
				Triple(arrCube, f, n, 2, 5, 8) ||
				Triple(arrCube, f, n, 0, 4, 8) ||
				Triple(arrCube, f, n, 2, 4, 6)
			  )
				return n;
		}
		return 0;
	}

	bool FaceFull(aibyte* arrCube, int nFace)
	{
		for(int i = 0; i < 9; ++i)
			if(GetFaceAt(arrCube, nFace, i) == 0)
				return false;
		return true;
	}


	int OppFace(int nFace)
	{
		int opp[6] = {1, 0, 3, 2, 5, 4};
		return opp[nFace];
	}

	int PlayerConvert(int nPl)
	{
		if(nPl == -1)
			return 2;
		return nPl;
	}

	void PrintCube(aibyte* arrCube)
	{
		int x,y;
		for(y = 0; y < 3; ++y)
		{   
			cout << "       ";
			for(x = 0; x < 3; ++x)
				cout << PlayerConvert(int(GetFaceAt(arrCube, 2, y*3 + x))) << " ";
			cout << "\n";
		}
	    
		cout << "\n";

		for(y = 0; y < 3; ++y)
		{   
			for(x = 0; x < 3; ++x)
				cout << PlayerConvert(int(GetFaceAt(arrCube, 0, y*3 + x))) << " ";
			cout << " ";
			for(x = 0; x < 3; ++x)
				cout << PlayerConvert(int(GetFaceAt(arrCube, 4, y*3 + x))) << " ";
			cout << " ";
			for(x = 0; x < 3; ++x)
				cout << PlayerConvert(int(GetFaceAt(arrCube, 1, y*3 + x))) << " ";
			cout << " ";
			cout << "\n";
		}

		cout << "\n";
	    
		for(y = 0; y < 3; ++y)
		{   
			cout << "       ";
			for(x = 0; x < 3; ++x)
				cout << PlayerConvert(int(GetFaceAt(arrCube, 3, y*3 + x))) << " ";
			cout << "\n";
		}

		cout << "\n";

		for(y = 0; y < 3; ++y)
		{   
			cout << "       ";
			for(x = 0; x < 3; ++x)
				cout << PlayerConvert(int(GetFaceAt(arrCube, 5, y*3 + x))) << " ";
			cout << "\n";
		}
	}

	Game::Game(int nCurrSide_, int nPlayer_)
	:nCurrSide(nCurrSide_), nPlayer(nPlayer_), bWin(false)
	{
		for(int i = 0; i < 27; ++i)
			arrCube[i] = 0;
	}

	void Game::ApplyMove(Move mv, Game& g)
	{
		memmove(g.arrCube, arrCube, 27);


		aibyte& b = GetFaceAt(g.arrCube, nCurrSide, mv.nMoveSquare);

#ifdef AI_DEBUG_MODE		
		if(b != 0)
			throw int(1);
#endif		

		b = nPlayer;

		g.bWin = WinCheck(arrCube, nCurrSide, mv.nMoveSquare, nPlayer);

		g.nCurrSide = GetMove(nCurrSide, mv.nTurnSide);
		g.nPlayer = OppositePlayer(nPlayer);
	}

	int Game::GetHeuristic()
	{
		return AntonAi::GetHeuristic(arrCube, PLAYER_ONE) + AntonAi::GetHeuristic(arrCube, PLAYER_TWO);
	}

	int Game::CheckForWin()
	{
		if(bWin)
			return OppositePlayer(nPlayer);

		if(FaceFull(arrCube, nCurrSide))
			return nPlayer;

		return VL_UNDEF;
	}

	  //int FaceOrderConversionId[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
	  //int FaceOrderConversionFw[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
	  //int FaceOrderConversionBk[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};

	  //int FaceOrderConversionId[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
		int FaceOrderConversionFw[] = {0, 2, 6, 4, 8, 1, 3, 5, 7};
		int FaceOrderConversionBk[] = {0, 5, 1, 6, 3, 7, 2, 8, 4};

		//int FaceOrderConversionFw[] = {0, 2, 6, 8, 1, 3, 5, 7, 4};
		//int FaceOrderConversionBk[] = {0, 4, 1, 5, 8, 6, 2, 7, 3};

	void Move::InitializeFromGame(Game& g)
	{
		int i;
		for(i = 0; i < 9; ++i)
			if(GetFaceAt(g.arrCube, g.nCurrSide, FaceOrderConversionFw[i]) == 0)
			{
				nMoveSquare = FaceOrderConversionFw[i];
				break;
			}
		
#ifdef AI_DEBUG_MODE		
		if(i == 9)
			throw int(1);
#endif
		nTurnSide = 0;

		pGm = &g;
	}

	bool Move::Next()
	{
		if(nTurnSide == 3)
		{
			nMoveSquare = FaceOrderConversionBk[nMoveSquare];
			
			int i;
			for(i = nMoveSquare + 1; i < 9; ++i)
				if(GetFaceAt(pGm->arrCube, pGm->nCurrSide, FaceOrderConversionFw[i]) == 0)
				{
					nMoveSquare = FaceOrderConversionFw[i];
					break;
				}

			if(i == 9)
				return true;

			nTurnSide = 0;
		}
		else
			++nTurnSide;
		
		return false;
	}
}

#pragma once
#include "QuConstants.h"
#include "QuDrawer.h"
#include "QuUtilities.h"
#include "QuCamera.h"

#ifdef TARGET_OF_IPHONE
#include "ofxiPhone.h"
#include "ofxiPhoneExtras.h"
#endif



//generic rendering context for full screen images with blending
class QuGuiRenderingContext
{
	QuGuiCamera mCamera;
public:
	QuGuiRenderingContext():mCamera(SCREEN_WIDTH,SCREEN_HEIGHT){}
	void enable(bool scale = true)
	{
		mCamera.setGuiProjection();
		if(scale)
			glScalef(mCamera.getWidth(),mCamera.getHeight(),1);
		glEnable(GL_BLEND);
		//glBlendFunc(GL_DST_ALPHA, GL_SRC_ALPHA);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(false);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
	}
	void disable()
	{
		glEnable(GL_LIGHTING);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(true);
		glDisable(GL_BLEND);
	}
};

//-------------
//blurring
//code taken from http://nehe.gamedev.net/data/lessons/lesson.asp?lesson=36
//-------------
#define WIDTH SCREEN_TEX_WIDTH
#define HEIGHT SCREEN_TEX_HEIGHT
class QuBlurrer
{
	QuGuiCamera mCamera;
	QuColorableDrawObject<int> mDraw;
	GLuint mTex;
	float mInc;
	float mRep;
private:
	void createDrawObject()
	{
		mDraw.setCount(4);
		float * aTexData = new float[4*2];
		memcpy(aTexData,GUI_RECT_UV,sizeof(float)*4*2);
		float * rectCoords = new float[4*3];
		memcpy(rectCoords,GUI_RECT_COORDS,sizeof(float) * 4 * 3);
		mDraw.loadVertices(rectCoords);
		mDraw.loadTexture(mTex,aTexData);
	}
	void captureImage()
	{
		glBindTexture(GL_TEXTURE_2D,mTex);
		glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, 0, 0, WIDTH, HEIGHT, 0);
	}
	void initialize()
	{			
		ofDisableArbTex();
		unsigned int* data;

		// Create Storage Space For Texture Data (128x128x4)
		data = (unsigned int*)new GLuint[((WIDTH * HEIGHT)* 4 * sizeof(unsigned int))];
		//ZeroMemory(data,((WIDTH * HEIGHT)* 4 * sizeof(unsigned int)));	// Clear Storage Memory

		glGenTextures(1, &mTex);								// Create 1 Texture
		glBindTexture(GL_TEXTURE_2D, mTex);					// Bind The Texture
		glTexImage2D(GL_TEXTURE_2D, 0, 4, WIDTH, HEIGHT, 0,
			GL_RGBA, GL_UNSIGNED_BYTE, data);						// Build Texture Using Information In data
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

		delete [] data;												// Release data
	}
public:
	QuBlurrer():mCamera(SCREEN_WIDTH,SCREEN_HEIGHT)
	{
		initialize();
	}
	~QuBlurrer()
	{
	}
	void setProperties(int reps,float inc)
	{
		mRep = reps;
		mInc = inc;
		//TODO generate colors
	}
	void capture()
	{
		captureImage();
		createDrawObject();
	}
	void draw()							// Draw The Blurred Image
	{
		glBlendFunc(GL_SRC_ALPHA,GL_ONE);							// Set Blending Mode
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glDisable(GL_DEPTH_TEST);									// Disable Depth Testing
		glEnable(GL_BLEND);											// Enable Blending

		glPushMatrix();
		mCamera.setGuiProjection();
		glScalef(WIDTH,HEIGHT,0);
		//glTranslatef(-(mCamera.getWidth() - WIDTH)/2.,-(mCamera.getHeight() - HEIGHT)/2.,0);
		//glScalef(50,50,50);
		glRotatef(90,0,0,1);
		mDraw.enable();
		for(int i = 0; i < mRep; i++)
		{
			glScalef(mInc,mInc,mInc);
			mDraw.draw();
		}
		mDraw.disable();
		glPopMatrix();

		glEnable(GL_DEPTH_TEST);									// Enable Depth Testing
		glDisable(GL_BLEND);										// Disable Blending
	}

};
#undef WIDTH
#undef HEIGHT




class QuTintableGuiImage
{
	QuGuiRenderingContext mContext;
	QuColorableDrawObject<int> mDrawObject;
	QuTimer mTimer;
	QuArrayInterpolator<float,4> mColor;
	bool mIsUseColor;
public:
	QuTintableGuiImage():mTimer(0,30),mIsUseColor(true)
	{
		float c[4] = {0.6,0.6,0.8,1};
		mColor.setBaseValue(c);
		float d[4] = {0.8,0.6,0.6,1};
		mColor.setTargetValue(d);
	}
	~QuTintableGuiImage()
	{
	}
	void useColor(bool aUseColor)
	{
		mIsUseColor = aUseColor;
		if(!mIsUseColor)
			mDrawObject.clearColors();
	}
	void update()
	{
		if(mIsUseColor)
		{
			mTimer.update();
			float * onec = mColor.interpolate(mTimer.getLinear());
			float * fourc = cpcpcpVector<float>(onec,4,4);
			delete [] onec;
			mDrawObject.addColor(1,fourc,4);
			mDrawObject.setKey(1);
		}
	}
	
	void unloadImage()
	{
		mDrawObject = QuColorableDrawObject<int>();
	}
		 
	bool loadImage(string aFilename, float * aTexData = NULL)
	{
		mDrawObject.setCount(4);
		if(aTexData == NULL)
		{
			aTexData = new float[4*2];
			memcpy(aTexData,GUI_RECT_UV,sizeof(float)*4*2);
		}
		if(!mDrawObject.loadImage(aFilename, aTexData))
			return false;
		//otherwise, if image loaded succesfully, we generate the rest of the info
		float * rectCoords = new float[4*3];
		memcpy(rectCoords,GUI_RECT_COORDS,sizeof(float) * 4 * 3);
		mDrawObject.loadVertices(rectCoords);
		return true;
	}
	void draw()
	{
		glPushMatrix();
		mContext.enable();
		mDrawObject.autoDraw();
		mContext.disable();
		glPopMatrix();
	}
	void draw(float x, float y, float rot, float s = 1)
	{
		glPushMatrix();
		mContext.enable(false);
		glScalef(1, 1, 1);
  		glTranslatef(-SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 0);
		glTranslatef(x,-y,0);
		glScalef(s,s,s);
		glRotatef(rot,0,0,1);
		mDrawObject.autoDraw();
		mContext.disable();
		glPopMatrix();
	}
	void setColor(float aColor[4])
	{
		mColor.setBaseValue(aColor);
		mColor.setTargetValue(aColor);
		update();
	}
	void setBaseColor(float aColor[4])
	{
		mColor.setBaseValue(aColor);
	}
	void setTargetColor(float aColor[4])
	{
		float * b = mColor.interpolate(mTimer.getLinear());
		float bs[4];
		memcpy(bs,b,sizeof(float)*4);
		setBaseColor(bs);
		delete [] b;
		mColor.setTargetValue(aColor);
		mTimer.reset();
	}
private:
};

struct QuConstantForceParticle
{
	float x,y,rot,s;
	float vx,vy,vr,vs;
	float ax,ay;
	QuTimer mTimer;
	QuArrayInterpolator<float,4> mColor;
	bool isUseColor;
	QuConstantForceParticle(unsigned profile = 0)
	{
		switch(profile)
		{
		case 1:
			isUseColor = true;
			x = y = ax = ay = vx = vy = rot = vr =0;
			s = 20;
			vs = 60;
			mTimer.setTargetAndReset(4);
			mColor.setBaseValue(FULL_OPAQUE);
			mColor.setTargetValue(FULL_TRANSPARENT);
			break;
		case 2:	//player 1 wins
			isUseColor = true;
			mTimer.setTargetAndReset(35);
			mColor.setBaseValue(FULL_RED_OPAQUE);
			mColor.setTargetValue(FULL_TRANSPARENT);
			x = y = 0;
			vx = quRandfSym()*30;
			vy = quRandfSym()*30;
			ax = 0;
			ay = 0;
			vr = quRandfSym()*3;
			s = 30;
			vs = -2;	//set to 20, pretty cool
			break;
		case 3: //player 2 wins;
		default:
			isUseColor = true;
			mTimer.setTargetAndReset(35);
			mColor.setBaseValue(FULL_BLUE_OPAQUE);
			mColor.setTargetValue(FULL_TRANSPARENT);
			x = y = 0;
			vx = quRandfSym()*15;
			vy = quRandfSym()*15;
			ax = 0;
			ay = 0;
			vr = quRandfSym()*3;
			s = 30;
			vs = -2;	//set to 20, pretty cool
			break;
		}
	}
	void setPosition(int argx, int argy)
	{
		x = argx; y = argy;
	}
	float * getColor()
	{
		if(!isUseColor)
			return FULL_OPAQUE;
		return mColor.interpolate(mTimer.getLinear());
	}
	void update()
	{
		mTimer.update();
		x += vx;
		y += vy;
		rot += vr;
		s += vs;

		vx += ax;
		vy += ay;
	}
	bool isInBox(QuRectangle<int> rect)
	{
		return rect.doesContainPoint(x,y);
	}
};

class QuFireworks
{
	QuTintableGuiImage mImg;
	list<QuConstantForceParticle> mParts;
	QuTimer mFireworksTimer;
	unsigned mPlayer;
public:
	QuFireworks()
	{
		mFireworksTimer.setTarget(150);
		mFireworksTimer.expire();
		mImg.loadImage("images/fireworks.png");
	}
	~QuFireworks()
	{
	}
	void fireworks(int x, int y)
	{
		//TODO play a sound
		for(int i = 0; i < 15; i++)
		{
			mParts.push_back(QuConstantForceParticle(2+mPlayer));
			mParts.back().setPosition(x,y);
			float r = HALF_PI;
			#ifdef TARGET_OF_IPHONE
			r = ofxAccelerometer.getForce().y;
			#endif
			mParts.back().ax = 5*cos(r);
			mParts.back().ay = 5*sin(r);
			
		}
	}
	void makeFireworks(unsigned aPlayer)
	{
		mPlayer = aPlayer;
		mFireworksTimer.reset();
	}
	void draw()
	{
		for(list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			if(i->isUseColor)
				mImg.setColor(i->getColor());
			mImg.draw(i->x,i->y,i->rot,i->s);
		}
	}
	void update()
	{

		//fireworks stuff
		switch(mFireworksTimer.getTimeSinceStart())
		{
		case 0:
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		case 10:
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		case 40:
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		case 65:
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		case 89:
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		case 120:
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		default:
			//if(mFireworksTimer.getTimeSinceStart() < 149 && quRandRange(0,4) == 0)
			//	fireworks(quRandRange(0,SCREEN_WIDTH),quRandRange(0,SCREEN_HEIGHT)); 
			break;
		}
		mFireworksTimer++;


		list<list<QuConstantForceParticle>::iterator> removal;
		for(list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			i->update();
			//TODO handle particle destruction
			int exp = 20;
			if(i->mTimer.isExpired())
				removal.push_back(i);
			else if(!i->isInBox(QuRectangle<int>(0-exp,0-exp,SCREEN_WIDTH + 2*exp,SCREEN_HEIGHT + 2*exp)))
				removal.push_back(i);
			
		}
		for(list<list<QuConstantForceParticle>::iterator>::iterator i = removal.begin(); i != removal.end(); i++)
			mParts.erase(*i);
	}
private:
};
class QuTouchEffect
{
	QuTintableGuiImage mImg;
	list<QuConstantForceParticle> mParts;
public:
	QuTouchEffect()
	{
		mImg.loadImage("images/transdot-01.png");
	}
	~QuTouchEffect()
	{
	}
	void touch(int x, int y)
	{
		for(int i = 0; i < 1; i++)
		{
			mParts.push_back(QuConstantForceParticle(1));
			mParts.back().setPosition(x,y);
		}
	}
	void draw()
	{
		for(list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			if(i->isUseColor)
				mImg.setColor(i->getColor());
			mImg.draw(i->x,i->y,i->rot,i->s);
		}
	}
	void update()
	{
		list<list<QuConstantForceParticle>::iterator> removal;
		for(list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			i->update();
			//TODO handle particle destruction
			int exp = 20;
			if(i->mTimer.isExpired())
				removal.push_back(i);
			else if(!i->isInBox(QuRectangle<int>(0-exp,0-exp,SCREEN_WIDTH + 2*exp,SCREEN_HEIGHT + 2*exp)))
				removal.push_back(i);
			
		}
		for(list<list<QuConstantForceParticle>::iterator>::iterator i = removal.begin(); i != removal.end(); i++)
			mParts.erase(*i);
	}
private:
};

class QuEffects
{
	QuTouchEffect mTouch;
public:
	QuEffects()
	{
	}
	~QuEffects()
	{
	}
	void draw()
	{

	}
	void update()
	{

	}
private:
};


inline void drawRectangle(QuRectangle<int> r, float color [4])
{
	QuGuiRenderingContext().enable(false);
	//invert the y
	glScalef(1,-1,1);
	glTranslatef(-SCREEN_WIDTH/2,-SCREEN_HEIGHT/2,0);
	//make the points
	float verts [4*3] = 
	{
		r.x, r.y, 0,
		r.x + r.w, r.y, 0,
		r.x, r.y + r.h, 0,
		r.x + r.w, r.y + r.h, 0
	};
	float colors [4 * 4] =
	{
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3]
	};

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glVertexPointer(3,GL_FLOAT,0,verts);
	glColorPointer(4,GL_FLOAT,0,colors);
	glDrawArrays(GL_LINE_STRIP,0,4);
	//glDrawArrays(GL_TRIANGLE_STRIP,0,4);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
}

class QuButtonGroup
{
	vector<QuRectangle<int> > mRects;
public:
	void addButton(QuRectangle<int> aR)
	{
		mRects.push_back(aR);

	}
	//returns -1 if nothing was pressed, otherwise the index of the vector
	int isButtonPressed(int x, int y)
	{
		for(int i = 0; i < mRects.size(); i++)
		{
			if(mRects[i].doesContainPoint(x,y))
				return i;
		}
		return -1;
	}
	void drawSelection()
	{
		QuGuiRenderingContext().enable(false);
		//invert the y
		glScalef(1,-1,1);
		glTranslatef(-SCREEN_WIDTH/2,-SCREEN_HEIGHT/2,0);
		for(int i = 0; i < mRects.size(); i++)
		{
			QuRectangle<int> r =  mRects[i];
			//make the points
			float verts [4*3] = 
			{
				r.x, r.y, 0,
				r.x + r.w, r.y, 0,
				r.x, r.y + r.h, 0,
				r.x + r.w, r.y + r.h, 0
			};
			/*
			float colors [4 * 4] =
			{
				1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1
			};*/

			float colors [4 * 4] =
			{
				0,0,0,1, 0,0,0,1, 0,0,0,1, 0,0,0,1,
			};

			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_COLOR_ARRAY);
			glVertexPointer(3,GL_FLOAT,0,verts);
			glColorPointer(4,GL_FLOAT,0,colors);
			glDrawArrays(GL_LINE_STRIP,0,4);
			//glDrawArrays(GL_TRIANGLE_STRIP,0,4);
			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_COLOR_ARRAY);
		}
	}
};
class QuMenuItem : public QuButtonGroup
{
	string mFn;
	bool isSet;
	bool lazyLoading;
	QuTintableGuiImage mImg;

public:
	QuMenuItem(bool ll = false)
	{
		lazyLoading = ll;
		isSet = false;
		mImg.useColor(false);
	}
	void loadImage()
	{
		if(isSet && lazyLoading)
			mImg.loadImage(mFn);
	}
	void setImage(string filename)
	{
		mFn = filename;
		isSet = true;
		if(!lazyLoading)
			mImg.loadImage(mFn);
	}
	void unloadImage()
	{
		mImg.unloadImage();
	}
	void draw()
	{
		mImg.draw();
	}
	void update()
	{
		//no need to update the image since it does nto change color
	}
};
class QuMenu
{
	list<QuMenuItem> mItems;
	unsigned mCur;
	
//READ ONLY
public:
	bool mIsUseAi;	//do we use ai?
	int mGameDiff;	//ai difficulty
	bool mIsPlayerFirst;	//player or ai first
	bool lazyLoading;

public:
//#ifndef OF_TARGET_IPHONE
#ifdef OF_TARGET_IPHONE
	QuMenu()
	{
		lazyLoading = false;
		mCur = 0;
		mItems.push_back(QuMenuItem(lazyLoading)); //0 main
		mItems.back().loadImage("images/title1.jpg");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));
		
		mItems.push_back(QuMenuItem(lazyLoading)); //1 one or 2 players
		mItems.back().loadImage("images/title2.jpg");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH/4,SCREEN_HEIGHT/3));
		mItems.back().addButton(QuRectangle<int>(0,SCREEN_HEIGHT/3,SCREEN_WIDTH/4,SCREEN_HEIGHT/3));

		mItems.push_back(QuMenuItem(lazyLoading)); //2 diff select
		mItems.back().loadImage("images/title3.jpg");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT/3));
		mItems.back().addButton(QuRectangle<int>(0,SCREEN_HEIGHT/3,SCREEN_WIDTH,SCREEN_HEIGHT/3));
		mItems.back().addButton(QuRectangle<int>(0,2*SCREEN_HEIGHT/3,SCREEN_WIDTH,SCREEN_HEIGHT/3));

		mItems.push_back(QuMenuItem(lazyLoading));	//3 first or seecond
		mItems.back().loadImage("images/moves.jpg");
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/4,SCREEN_HEIGHT/3,3*SCREEN_WIDTH/4,SCREEN_HEIGHT/3));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/4,0,3*SCREEN_WIDTH/4,SCREEN_HEIGHT/3));

		mItems.push_back(QuMenuItem(lazyLoading));	//4 credits
		mItems.back().loadImage("images/blend_color.jpg");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));

		mItems.push_back(QuMenuItem(lazyLoading));	//5 tutorial
		mItems.back().loadImage("images/blend_color.jpg");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));
	}
#else
	QuMenu()
	{
		lazyLoading = false;
		mCur = 0;
		mItems.push_back(QuMenuItem(lazyLoading)); //0 main
		mItems.back().setImage("images/title1.jpg");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));
		//load the first image to be displayed
		mItems.back().loadImage();
		
		mItems.push_back(QuMenuItem(lazyLoading)); //1 one or 2 players
		mItems.back().setImage("images/title2.jpg");
		mItems.back().addButton(QuRectangle<int>(0,3*SCREEN_HEIGHT/4,SCREEN_WIDTH/3,SCREEN_HEIGHT/4));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/3,3*SCREEN_HEIGHT/4,SCREEN_WIDTH/3,SCREEN_HEIGHT/4));

		mItems.push_back(QuMenuItem(lazyLoading)); //2 diff select
		mItems.back().setImage("images/title3.jpg");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH/3,SCREEN_HEIGHT*4/5));
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/3,0,SCREEN_WIDTH/3,SCREEN_HEIGHT*4/5));
		mItems.back().addButton(QuRectangle<int>(2*SCREEN_WIDTH/3,0,SCREEN_WIDTH/3,SCREEN_HEIGHT*4/5));

		mItems.push_back(QuMenuItem(lazyLoading));	//3 first or seecond
		mItems.back().setImage("images/moves.jpg");
		mItems.back().addButton(QuRectangle<int>(SCREEN_WIDTH/3,0,SCREEN_WIDTH/3,SCREEN_HEIGHT*3/4));
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH/3,SCREEN_HEIGHT*3/4));

		mItems.push_back(QuMenuItem(lazyLoading));	//4 credits
		mItems.back().setImage("images/blend_color.jpg");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));

		mItems.push_back(QuMenuItem(lazyLoading));	//5 tutorial
		mItems.back().setImage("images/blend_color.jpg");
		mItems.back().addButton(QuRectangle<int>(0,0,SCREEN_WIDTH,SCREEN_HEIGHT));
	}
#endif
	~QuMenu()
	{
	}
	bool isMenuOver()
	{
		return mCur >= mItems.size();
	}
	list<QuMenuItem>::iterator getCurrent()
	{
		list<QuMenuItem>::iterator it = mItems.begin();
		if(isMenuOver())
			return mItems.end();
		for(int i = 0; i < mCur; i++)
			it++;
		return it;
	}
	void resetMenu()
	{
		mCur = 0;
		getCurrent()->loadImage();
	}
	void drawSelection()
	{
		if(!isMenuOver())
			getCurrent()->drawSelection();
	}
	void draw()
	{
		if(!isMenuOver())
			getCurrent()->draw();
	}
	void update()
	{
		//no need to update the buttons
	}
	//TODO finish
	void mouseReleased(int x, int y)
	{
		if(isMenuOver())
			return;		
		unsigned v = getCurrent()->isButtonPressed(x,y);
		cout << "clicked id " << v << " current menu " << mCur << endl;
		if(v == -1)
			return;
		if(lazyLoading)
			getCurrent()->unloadImage();
		switch(mCur)
		{
		case 0:	//press to start
			mCur = 1;
			break;
		case 1:	//one or two players
			if( v == 1)	//two
			{
				mIsUseAi = false;
				mCur = mItems.size(); 
				break;
			}
			mIsUseAi = true;
			mCur = 2;
			break;

		case 2:	//difficulty select
			mGameDiff = v; //0 easy, 1 medium, 2 hard
			mCur = 3;
			break;
		case 3:	//first or second
			mIsPlayerFirst = v; //0 cpu first, 1 player first
			mCur = mItems.size();
			break;
		case 4: //credits
			mCur = 0;
			break;
		case 5: //tutorial
			mCur = 0;
			break;
		default: break;
		}
		if(isMenuOver())
			return;
		getCurrent()->loadImage();
	}
private:
};

class QuTransition
{
public:
	virtual void update(){}
	virtual void draw(){}
	virtual bool isFinished() = 0;
};

class QuSlidingTransition : QuTransition
{
	QuTintableGuiImage mImg;
	QuTimer mSlideTimer;
	QuTimer mPauseTimer;
	QuArrayInterpolator<float,4> mColor;
public:
	QuSlidingTransition():mSlideTimer(0,10),mPauseTimer(0,8)
	{
		mColor.setBaseValue(THREE_QUARTER_OPAQUE);
		mColor.setTargetValue(FULL_TRANSPARENT);
		mPauseTimer.expire();
	}
	virtual void update()
	{
		if(mPauseTimer.isExpired())
		{
			mSlideTimer--;
		}
		else if(mSlideTimer.isExpired())
		{
			mPauseTimer++;
		}
		else 
		{
			mSlideTimer++;
		}
	}
	void loadImage(string filename)
	{
		mImg.loadImage(filename);
	}
	void reset()
	{
		mPauseTimer.reset();
		mSlideTimer.reset();
	}
	virtual void draw()
	{
		//TODO calculate position and stuff
		float y;
		if(!mPauseTimer.isExpired() && !mSlideTimer.isExpired())
			y = (mSlideTimer.getSquareRoot()-1);
		else if(!mPauseTimer.isExpired() && mSlideTimer.isExpired())
			y = 0;
		else
			y = (1-mSlideTimer.getSquare());
		mImg.setColor(mColor.interpolate(quAbs<float>(y)));
		//mImg.draw((y)*SCREEN_WIDTH/2+SCREEN_WIDTH/2,SCREEN_HEIGHT/2,0,250);
		mImg.draw(SCREEN_WIDTH/2,y*SCREEN_HEIGHT/2+ SCREEN_HEIGHT/2,0,250);
	}
	virtual bool isFinished()
	{
		return mPauseTimer.isExpired() && mSlideTimer.getLinear() == 0;
	}
};

class QuOverlayer
{
	map<int,QuSlidingTransition> mParts;
	int mPrevPlayer;
public:
	QuOverlayer()
	{
		mPrevPlayer = 0;
		mParts[0] = QuSlidingTransition();
		mParts[0].loadImage("images/red_turn.png");
		mParts[1] = QuSlidingTransition();
		mParts[1].loadImage("images/blue_turn.png");
		
	}
	~QuOverlayer()
	{
	}
	void setOverlay(int player)
	{
		if( player != mPrevPlayer)
			mParts[player].reset();
		mPrevPlayer = player;
	}
	void draw()
	{
		for(map<int,QuSlidingTransition>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			if(!i->second.isFinished())
			{
				i->second.draw();
			}
		}
	}
	void update()
	{
		for(map<int,QuSlidingTransition>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			i->second.update();
		}
	}
private:
};


class QuBackground
{
	QuArrayInterpolator<float,4> mRed;
	QuArrayInterpolator<float,4> mBlue;
	QuTimer mColorTimer;
	QuTintableGuiImage mImg;
	//TODO make this happen with tinting alpha
	QuTintableGuiImage mArrows;
	char mPreviousPlayer;
	char mPreviousMoveType;
private:
	void setPlayer(char aPlayer)
	{
		if(mPreviousPlayer != aPlayer)
		{
			//not set target color automatically sets the base color
			//if player is red (P1)
			if(aPlayer == 0)
				mImg.setTargetColor(mRed.interpolate(mColorTimer.getLinear()));
			else
				mImg.setTargetColor(mBlue.interpolate(mColorTimer.getLinear()));
			mColorTimer++;
		}
		mPreviousPlayer = aPlayer;
	}
	void setMoveType(char aMove)
	{
		if(mPreviousMoveType != aMove)
		{
			//turn on arrows
			if(aMove == 1)
				mArrows.setTargetColor(ONE_EIGTH_OPAQUE);
			//turn off arrows
			else
				mArrows.setTargetColor(FULL_TRANSPARENT);
		}
		mPreviousMoveType = aMove;
	}
public:
	QuBackground():mColorTimer(0,STEPS_TILL_FULL_DARK)
	{
		mPreviousMoveType = 0;
		mPreviousPlayer = 0;

		mRed.setBaseValue(STARTING_RED_COLOR);
		mRed.setTargetValue(ENDING_RED_COLOR);
		mBlue.setBaseValue(STARTING_BLUE_COLOR);
		mBlue.setTargetValue(ENDING_BLUE_COLOR);
		mImg.loadImage("images/background_02.png");
		mArrows.loadImage("images/TURNICON_IPHONE4.png");

		mArrows.setTargetColor(FULL_TRANSPARENT);
		mArrows.setBaseColor(FULL_TRANSPARENT);

		mImg.update();
		mArrows.update();
	}
	~QuBackground()
	{
	}
	void reset()
	{
		mColorTimer.reset();
		mImg.setTargetColor(mRed.interpolate(mColorTimer.getLinear()));
	}
	void setState(QuGameState aState)
	{
		setPlayer(stateToPlayer(aState));
		setMoveType(stateToMoveType(aState));
	}
	void draw()
	{
		mImg.draw();
		mArrows.draw();
	}
	void update()
	{
		mImg.update();
		mArrows.update();
	}
private:
};
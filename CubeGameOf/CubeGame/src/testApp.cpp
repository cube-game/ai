#include "testApp.h"
#include "QuDrawer.h"
#include "QuCube.h"
#include "QuInterface.h"
#include "QuSound.h"
//--------------------------------------------------------------
void testApp::setup(){

	/*QuQuaternion s = computeDeviceRotation(0.1,0,0);
	cout << s.x << " " << s.y << " " << s.z << " " << s.w << endl;*/


	ofSetFrameRate(30);
	ofBackground(0,0,0);
	ofDisableSetupScreen();
	ofSetWindowShape(SCREEN_WIDTH,SCREEN_HEIGHT);
	iphoneSetup();

	mCam = new QuCamera(ofGetWidth(),ofGetHeight());
	mCube = new QuCube();
	mDraw = new QuDrawer();
	mBg = new QuBackground();
	mMenu = new QuMenu();
	mTouchEffect = new QuTouchEffect();
	mFireworks = new QuFireworks();
	mOverlay = new QuOverlayer();
	mBlur.setProperties(1,1.1);
	mGameRestartTimer.setTargetAndReset(180);

	isMenu = true;

	//TODO should figure out how to set this to false
	ofSetBackgroundAuto(true);


	//load my sounds
#ifndef TARGET_OF_IPHONE
	QuSoundManager::getRef().loadSound(SOUND_BG,true);
#endif
	QuSoundManager::getRef().loadSound(SOUND_NEXT_TURN,false);
	QuSoundManager::getRef().loadSound(SOUND_WINNER,false);
	QuSoundManager::getRef().loadSound(SOUND_HIGHLIGHT,false);
	QuSoundManager::getRef().loadSound(SOUND_SELECT,false,false,true);
}

void testApp::testRoutine()
{
	QuDrawObject mSphereDrawObject;
	int sub = 6;
	int iter = 1;
	for(int i = 0; i < sub; i++)
		iter*=4;
	mSphereDrawObject.setCount(8*3*iter);
	float * v = NULL;
	//v = new float[8*3*3];
	//memcpy(v,OCTAHEDRON_TRIANGLE_COORDS,sizeof(float)*8*3*3);
	//printMatrix<float>(v,8*3,3);
	generateIcosphere(v,v,v,sub);
	mSphereDrawObject.loadVerticesAndGenerateNormals(v);
	mSphereDrawObject.setDrawType(GL_TRIANGLES);

	//glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	mSphereDrawObject.enable();
	mSphereDrawObject.draw();
	mSphereDrawObject.disable();

}
//--------------------------------------------------------------

void testApp::drawMenu()
{
	mMenu->draw();
	//mMenu->drawSelection();
}
void testApp::drawGame()
{
	mBg->draw();
	mFireworks->draw();
	mCam->setHeightAndDistanceFromRotation();
	mCam->setCubeModelProjection();
	mDraw->computeFrontFacingFaces(*mCam);
	mDraw->drawCube(*mCube,mCube->getCurrentFace());
	mOverlay->draw();
	//mBlur.capture();
	//mBlur.draw();
	//mDraw->drawSelectionFaces(mCube->getCurrentFace(),0,0);
}
void testApp::updateMenu()
{
	if(mMenu->isMenuOver())
	{
		if(mMenu->mIsUseAi)
		{
			if(mMenu->mIsPlayerFirst)
				mProf2.createAi(mMenu->mGameDiff);
			else
				mProf1.createAi(mMenu->mGameDiff);
		}
		//start the game
		isMenu = false;
		QuSoundManager::getRef().setFade(SOUND_BG,true);
		QuSoundManager::getRef().playSound(SOUND_BG,true);
	}
}
void testApp::updateGame()
{
	mBg->setState(mCube->getCurrentState());
	mBg->update();
	mCam->update();
	mOverlay->update();
	mFireworks->update();
	mAccel.update(); 
	mMouse.update();
	tiltUpdate();

	//decide if we need to do our ai business
	if(mProf1.isAi())
		mProf1.getAi().update(*mCube);
	if(mProf2.isAi())
		mProf2.getAi().update(*mCube);

	//update rotation if ai has moved
	mCam->rotateInDirection(mCube->getAndResetLastTurnDirection());

	//if turn has changed, tell qu overlayer to do its business
	mOverlay->setOverlay(mCube->getCurrentPlayer());

	//decide if we want to restart the game
	if(mCube->getCurMoveType() == 2)
	{
		//fade out the music
		//if(!QuSoundManager::getRef().isFadingOut(SOUND_BG))
		//	QuSoundManager::getRef().setFade(SOUND_BG,false);
		if(mGameRestartTimer.getTimeSinceStart() == 1)
		{
			mFireworks->makeFireworks(mCube->getCurrentPlayer());
			QuSoundManager::getRef().setFade(SOUND_BG,false);
		}
		mGameRestartTimer.update();
		if(mGameRestartTimer.isExpired())
		{
			mGameRestartTimer.reset();
			mCube->reset();
			mCam->reset();
			mBg->reset();
			mMenu->resetMenu();
			mProf1.reset();
			mProf2.reset();
			QuSoundManager::getRef().stopSound(SOUND_BG);
			isMenu = true;
		}
	}
}
void testApp::releaseMenu(int x, int y)
{
	mMenu->mouseReleased(x,y);
}
void testApp::releaseGame(int x, int y)
{
	mCam->setCubeModelProjection();
	if( (!mProf1.isAi() && mCube->getCurrentPlayer() == 0) ||
		(!mProf2.isAi() && mCube->getCurrentPlayer() == 1))
	{
		unsigned face = mDraw->drawSelectionFaces(mCube->getCurrentFace(),x,y);
		if(face == 13)
			mCube->unHighlightCube();
		if(mCube->isMoveHighlighted(face))
		{
			mCube->attempMakeMove(face);
		}
		else
		{
			mCube->highlightMove(face);
		}
		QuTurnDirections dir = mMouse.mouseReleased(x,y);
		if(dir != IDENTITY_DIRECTION && !mCube->attemptTurnToFace(dir))
		{
			//TODO play a sound
			float mag = 2000;
			switch(dir)
			{
			case LEFT:
				mCam->addImpulse(-mag,0);
				break;
			case RIGHT:
				mCam->addImpulse(mag,0);
				break;
			case UP:
				mCam->addImpulse(0,-mag);
				break;
			case DOWN:
				mCam->addImpulse(0,mag);
				break;
			default:
				break;
			}
		}
		mCam->rotateInDirection(mCube->getAndResetLastTurnDirection());
	}
}
void testApp::pressGame(int x, int y)
{
	mCam->setCubeModelProjection();
	if(mDraw->drawSelectionFaces(mCube->getCurrentFace(),x,y) != 13)
	#ifdef TARGET_OF_IPHONE
		mCam->addImpulse(SCREEN_WIDTH/2 - x*2,SCREEN_HEIGHT/2 - y*2);
	#else
		mCam->addImpulse(SCREEN_WIDTH/2 - x,SCREEN_HEIGHT/2 - y);
	#endif
	mMouse.mousePressed(x,y);
}

void testApp::update()
{
	QuSoundManager::getRef().update();
	mTouchEffect->update();
	if(isMenu)
		updateMenu();
	else
		updateGame();
	//use me as a timer
	//ofGetFrameNum();
	//cout << gluErrorString(glGetError()) << endl;
}

void testApp::draw(){
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	if(isMenu)
		drawMenu();
	else
		drawGame();
	mTouchEffect->draw();
}

void testApp::keyPressed(int key){
	mAccel.keyPressed(key);
}

void testApp::keyReleased(int key){
	mAccel.keyReleased(key);
}
 
void testApp::mousePressed(int x, int y, int button){
	#ifdef TARGET_OF_IPHONE
	mTouchEffect->touch(2*x,2*y);
	if(!isMenu)
		pressGame(x,y);
	#else
	mTouchEffect->touch(x,y);
	if(!isMenu)
		pressGame(x,y);
	#endif
}

void testApp::mouseReleased(int x, int y, int button){
	#ifdef TARGET_OF_IPHONE
	if(isMenu)
		releaseMenu(2*x,2*y);
	else
		releaseGame(x,y);
	#else
	if(isMenu)
		releaseMenu(x,y);
	else
		releaseGame(x,y);
	#endif
}

void testApp::mouseDragged(int x, int y, int button)
{
	#ifdef TARGET_OF_IPHONE
	if(!isMenu)
		mMouse.mouseDragged(2*x,2*y);
	#else
	if(!isMenu)
		mMouse.mouseDragged(x,y);
	#endif
	
}
//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
	//reset screen center
}




//-------------------------
//iphone stuff
//-------------------------

void testApp::iphoneSetup()
{
#ifdef TARGET_OF_IPHONE
	// register touch events
	ofxRegisterMultitouch(this);
	
	// initialize the accelerometer
	ofxAccelerometer.setup();
	
	//iPhoneAlerts will be sent to this.
	ofxiPhoneAlerts.addListener(this);
#endif
}

void testApp::tiltUpdate()
{
	
#ifdef TARGET_OF_IPHONE
	//mAccelMan.update(ofxAccelerometer.getForce().y,ofxAccelerometer.getForce().x,ofxAccelerometer.getForce().z);	
	//Cam->setTiltTarget(computeDeviceRotation(ofxAccelerometer.getForce().y,ofxAccelerometer.getForce().x,ofxAccelerometer.getForce().z));
	//mCam->setTiltTarget(mAccelMan.getX(),mAccelMan.getY());	
	mCam->setTiltTarget(ofxAccelerometer.getForce().y,ofxAccelerometer.getForce().x,ofxAccelerometer.getForce().z);	
	mCam->adjustTiltTarget(-mMouse.getChange().x/30.,mMouse.getChange().y/40.);
#else
	mCam->setTiltTarget(mAccel.getY(),mAccel.getX());
	mCam->adjustTiltTarget(-mMouse.getChange().x/100.,mMouse.getChange().y/200.);
#endif
}

#ifdef TARGET_OF_IPHONE
void testApp::touchDown(int x, int y, int id){
	//printf("touch %i down at (%i,%i)\n", id, x,y);
}

void testApp::touchMoved(int x, int y, int id){
	//printf("touch %i moved at (%i,%i)\n", id, x, y);
}

void testApp::touchUp(int x, int y, int id){
	printf("touch %i up at (%i,%i)\n", id, x, y);
}

void testApp::touchDoubleTap(int x, int y, int id){
	//printf("touch %i double tap at (%i,%i)\n", id, x, y);
}

void testApp::lostFocus() {
}

void testApp::gotFocus() {
}

void testApp::gotMemoryWarning() {
	cout << "MEMORY WARNING" << endl;
}

void testApp::deviceOrientationChanged(int newOrientation){
}
#endif


#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"
#include "QuConstants.h"

//========================================================================
int main( ){

    ofAppGlutWindow window;
	//ofSetupOpenGL(&window, 480,320, OF_WINDOW);			// <-------- setup the GL context
	ofSetupOpenGL(&window, SCREEN_WIDTH,SCREEN_HEIGHT, OF_WINDOW);
	//ofSetupOpenGL(&window, 1024,768, OF_WINDOW);			// <-------- setup the GL context
	//ofSetupOpenGL(&window, 1920,1080, OF_WINDOW);			// <-------- setup the GL context

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp( new testApp());
}

# The name of our project is "CUBEMATH". CMakeLists files in this project can 
# refer to the root source directory of the project as ${CUBEMATH_SOURCE_DIR} and 
# to the root binary directory of the project as ${CUBEMATH_BINARY_DIR}. 
cmake_minimum_required (VERSION 2.4) 

project (CUBEMATH)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)

add_library(CUBEMATH 
			Assert.c
			Assert.h
			intersect.cpp
			intersect.h
			line2.h
			Line3.h
			Matrix2.h
			Matrix3.h
			matrix4.h
			MatrixN.h
			plane.h
			quaternion.h
			random.h
			Rect.h
			Spline.cpp
			Spline.h
			Utility.h
			vector2.h
			vector3.h
			vector4.h
		)


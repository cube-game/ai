#include <limits.h>
#include <vector>
#include <map>
#include <list>
#include <algorithm>
#include <iostream>

namespace AntonAi
{
	const int VL_UNDEF = INT_MAX - 1;
	const int VL_DOMINATE = INT_MAX - 3;

	const int PLAYER_MAX = 1;
	const int PLAYER_MIN = -1;
	const int PLAYER_ONE = PLAYER_MAX;
	const int PLAYER_TWO = PLAYER_MIN;

	const int WIN_HEURISTIC = 1000;

	inline void AiAssert(bool b)
	{
		if(!b)
			throw int(0);
	}

	template<class T>
	void AiAssert(T n)
	{
		AiAssert(n != 0);
	}

	inline int OppositePlayer(int nPlayer)
	{
		return -nPlayer;
	}

	struct Game;

	struct Move
	{
		int nMoveSquare;
		int nTurnSide;

		Game* pGm;

		void InitializeFromGame(Game& g);
		
		bool Next();

		Move():pGm(0){}
	};

	struct Game
	{
		signed char arrCube[27];
		int nCurrSide;
		int nPlayer;

		bool bWin;

		Game():bWin(false){}
		Game(int nCurrSide_, int nPlayer_);

		void ApplyMove(Move mv, Game& g);

		int GetHeuristic();

		int CheckForWin();
	};

	void PrintCube(signed char* arrCube);

	void PrintGame(Game* pGm);

	inline int Best(int nPlayer, int nV1, int nV2)
	{
		if(nV1 == VL_DOMINATE)
			return nV2;
		if(nV2 == VL_DOMINATE)
			return nV1;
		if(nV1 == VL_UNDEF)
			return nV2;
		if(nV2 == VL_UNDEF)
			return nV1;

		if(nPlayer == PLAYER_MAX)
			return std::max(nV1, nV2);
		else if(nPlayer == PLAYER_MIN)
			return std::min(nV1, nV2);
#ifdef AI_DEBUG_MODE		
		else
			throw int(0);
#endif		
	}

	struct Node
	{
		Game gm;

		int nPlayer;

		int nThisPlayerBranchVal;
		int nThisPlayerGlobalVal;
		int nOtherPlayerGlobalVal;

		Move MvCurrent;
		int nMoveVal;

		Node(){}

		Node(int nPlayer_):nPlayer(nPlayer_), nThisPlayerBranchVal(VL_UNDEF),
							nThisPlayerGlobalVal(VL_UNDEF), nOtherPlayerGlobalVal(VL_UNDEF),
							nMoveVal(VL_UNDEF)
		{}
	};

	enum AiDifficulty {AD_EASY, AD_MEDIUM, AD_HARD};

	struct Statistics
	{
		std::map <int, std::vector<Move> > mMoves;
		
		int nPlayer;
		AiDifficulty ad;

		Statistics(int nPlayer_, AiDifficulty ad_)
			:nPlayer(nPlayer_), ad(ad_){}

		void AddMove(int nVal, Move mv)
		{
			mMoves[nVal].push_back(mv);
		}

		int GetBestValue()
		{
#ifdef AI_DEBUG_MODE		
			if(mMoves.empty())
				throw int (-1);
#endif
			
			if(nPlayer == -1)
				return mMoves.begin()->first;
			else if(nPlayer == 1)
				return mMoves.rbegin()->first;
#ifdef AI_DEBUG_MODE		
			else
				throw int(-2);
#endif
		}

		Move GetMove()
		{
#ifdef AI_DEBUG_MODE		
			if(mMoves.empty())
				throw int (-1);
#endif
			if(nPlayer == -1)
				return GetMove(mMoves.begin(), mMoves.end());
			else if(nPlayer == 1)
				return GetMove(mMoves.rbegin(), mMoves.rend());
#ifdef AI_DEBUG_MODE		
			else
				throw int(-2);
#endif
		}

		template <class T>
		Move GetMove(T begin, T end)
		{
			std::vector<Move> vMoves;
			std::vector<Move> vAllMoves;

			if(ad == AD_HARD)
			{
				Append(vMoves, begin->second);
			}
			else if (ad == AD_MEDIUM)
			{
				Append(vMoves, begin->second);
				
				if(begin->first != ( 1) * nPlayer * WIN_HEURISTIC)
				{
					++begin;
					
					if(begin != end)
						Append(vMoves, begin->second);
				}
			}
			else if (ad == AD_EASY)
			{
				while(begin != end)
				{
					if(begin->first != (-1) * nPlayer * WIN_HEURISTIC)
						Append(vMoves, begin->second);
					Append(vAllMoves, begin->second);
					
					if(begin->first == ( 1) * nPlayer * WIN_HEURISTIC)
						break;
					
					++begin;
				}
			}
#ifdef AI_DEBUG_MODE		
			else
				throw int(-3);
#endif		

			if(vMoves.empty() && ad == AD_EASY)
				vMoves = vAllMoves;

#ifdef AI_DEBUG_MODE		
			if(vMoves.empty())
				throw int(-4);
#endif
			std::cout << "Available moves: " << vMoves.size() << "\n";
			
			return vMoves[rand()%vMoves.size()];
		}

		void Append(std::vector<Move>& vTarget, std::vector<Move>& vSource)
		{
			for(unsigned i = 0; i < vSource.size(); ++i)
				vTarget.push_back(vSource[i]);
		}

		void PrintMap()
		{
			for(std::map <int, std::vector<Move> >::iterator itr = mMoves.begin(), etr = mMoves.end();
				itr != etr; ++itr)
			{
				std::cout << itr->first << " " << itr->second.size() << " : ";
			}
		}
	};

	template <bool bDominate>
	void ProcessNode(std::vector<Node>& v, int nMaxSize, Statistics& st)
	{
#ifdef AI_DEBUG_MODE		
		AiAssert(v.size());
#endif		
		Node& rNd = v.back();

#ifdef AI_DEBUG_MODE		
		AiAssert(rNd.nThisPlayerGlobalVal != VL_DOMINATE);
		AiAssert(rNd.nOtherPlayerGlobalVal != VL_DOMINATE);
#endif		

		if(rNd.nMoveVal != VL_UNDEF)
		{				
			if(rNd.nMoveVal == VL_DOMINATE)
			{
				if(rNd.nThisPlayerBranchVal == VL_UNDEF)
					rNd.nThisPlayerBranchVal = VL_DOMINATE;
			}
			else
			{
				if(v.size() == 1)
				{
#ifdef AI_DEBUG_MODE		
					AiAssert(rNd.nThisPlayerGlobalVal == VL_UNDEF);
					AiAssert(rNd.nOtherPlayerGlobalVal == VL_UNDEF);
#endif		
					
					
					/*
					if(rNd.nMoveVal == rNd.nThisPlayerBranchVal)
					{
						st.nBestVal = rNd.nMoveVal;
						st.vBestMoves.push_back(rNd.MvCurrent);
					}
					else if(Best(rNd.nPlayer, rNd.nThisPlayerBranchVal, rNd.nMoveVal) == rNd.nMoveVal)
					{
						st.nBestVal = rNd.nMoveVal;
						st.vBestMoves.clear();
						st.vBestMoves.push_back(rNd.MvCurrent);
					}
					*/

					st.AddMove(rNd.nMoveVal, rNd.MvCurrent);
				}

				rNd.nThisPlayerBranchVal = Best(rNd.nPlayer, rNd.nThisPlayerBranchVal, rNd.nMoveVal);
			}
			
#ifdef AI_DEBUG_MODE		
			AiAssert(rNd.nThisPlayerBranchVal != VL_UNDEF);
#endif
			bool bDone = (rNd.nMoveVal == WIN_HEURISTIC * rNd.nPlayer) || rNd.MvCurrent.Next();

			if(bDone)
			{
				if(bDominate)
				{
					
					if(Best(rNd.nPlayer, rNd.nThisPlayerGlobalVal, rNd.nThisPlayerBranchVal) != rNd.nThisPlayerBranchVal//)
						|| rNd.nThisPlayerGlobalVal == rNd.nThisPlayerBranchVal) // whether we dump turns of equal value
					{
						v.pop_back();

#ifdef AI_DEBUG_MODE		
						AiAssert(v.size());
#endif
						v.pop_back();
#ifdef AI_DEBUG_MODE		
						AiAssert(v.size());
#endif
						v.back().nMoveVal = VL_DOMINATE;
						return;
					}

				}

				int nRet = rNd.nThisPlayerBranchVal;

				v.pop_back();

				if(v.empty())
				{
#ifdef AI_DEBUG_MODE		
					AiAssert(nRet != VL_DOMINATE);
#endif
					return;		// finished analysis
				}

				if(nRet == VL_DOMINATE)
				{
					v.pop_back();
#ifdef AI_DEBUG_MODE		
					AiAssert(v.size());
#endif
					v.back().nMoveVal = VL_DOMINATE;
				}
				else
					v.back().nMoveVal = nRet;

				return;
			}
		}
		
		v.push_back(Node());

		Node& rNewNd = v.back();

		rNewNd.nPlayer = OppositePlayer(rNd.nPlayer);
		rNewNd.nThisPlayerBranchVal = VL_UNDEF;
		rNewNd.nThisPlayerGlobalVal = rNd.nOtherPlayerGlobalVal;
		rNewNd.nOtherPlayerGlobalVal = Best(rNd.nPlayer, rNd.nThisPlayerBranchVal, rNd.nThisPlayerGlobalVal);
		rNewNd.nMoveVal = VL_UNDEF;

#ifdef AI_DEBUG_MODE		
		AiAssert(rNewNd.nOtherPlayerGlobalVal != VL_DOMINATE);
#endif
		rNd.gm.ApplyMove(rNd.MvCurrent, rNewNd.gm);

		// Check for instant win

		int nWinPlayer = rNewNd.gm.CheckForWin();

		if(nWinPlayer != VL_UNDEF)
		{
			rNd.nMoveVal = nWinPlayer * WIN_HEURISTIC;
			v.pop_back();
			return;
		}

		rNewNd.MvCurrent.InitializeFromGame(rNewNd.gm);

		// check for depth

		if(v.size() == (nMaxSize + 1))
		{
			rNd.nMoveVal = rNewNd.gm.GetHeuristic();
			v.pop_back();
			return;
		}

	}


	struct IterativeStrategy
	{
		std::vector<Statistics> vResults;

		Game gm;
		std::vector<Node> vNodes;
		Statistics st;

		int nCurrDepth;
		int nMaxDepth;

		bool bDoneAnalysis;

		AiDifficulty ad;

		void InitializeNewGame();

		IterativeStrategy(Game gm_, AiDifficulty ad_);

		void MultiStep(unsigned nNum)
		{
			if(ad == AD_HARD)
			{
				for(unsigned i = 0; i < nNum; ++i)
					Step<true>();
			}
			else
			{
				for(unsigned i = 0; i < nNum; ++i)
					Step<false>();
			}

		}
		
		template <bool bDominate>
		bool Step()
		{
			if(vNodes.size())
				ProcessNode<bDominate>(vNodes, nCurrDepth, st);
			else
			{	
				if(!bDoneAnalysis)
				{

					if(
						Best(gm.nPlayer, st.GetBestValue(), gm.nPlayer * (WIN_HEURISTIC - 100) )
						== st.GetBestValue())
					{
						std::cout << "Win termination\n";
						
						vResults.push_back(st);
						bDoneAnalysis = true;
					}
					else if(
						Best(-gm.nPlayer, st.GetBestValue(), (-gm.nPlayer) * (WIN_HEURISTIC - 100) )
						== st.GetBestValue())
					{
						std::cout << "Lose termination\n";
						
						if(vResults.empty())
							vResults.push_back(st);
						bDoneAnalysis = true;
					}
					else
					{
						std::cout << "Finised depth " << nCurrDepth << "\n";
						
						vResults.push_back(st);
						++nCurrDepth;

						if(nCurrDepth > nMaxDepth)
						{
							std::cout << "Depth termination\n";
							bDoneAnalysis = true;
						}
					}
				}

				InitializeNewGame();

				return true;
			}
			return false;
		}


		Move GetMove();
	};
}

namespace AntonAi
{

	inline int GetMove(int nFace, int n)
	{
		int move[6*4] = 
						{2, 3, 4, 5, 2, 3, 4, 5,
						 0, 1, 4, 5, 0, 1, 4, 5,
						 0, 1, 2, 3, 0, 1, 2, 3};
		return move[nFace*4 + n];
	}
}
#include "aicube.h"

int* Cube::at(vector<int> crd)
{
    return &(vEls.at(crd.at(0) + 3 * crd.at(1) + 9 * crd.at(2)));
}

int* Cube::at(int x, int y, int z)
{
    std::vector<int> v;
    v.push_back(x);
    v.push_back(y);
    v.push_back(z);
    return at(v);
}


vector<int*> Cube::GetFace(vector<int> crd)
{
    vector<int*> vRet;
    
    vector<int> itr(3, 0);
    for(itr[2] = 0; itr[2] < 3; ++itr[2])
    for(itr[1] = 0; itr[1] < 3; ++itr[1])
    for(itr[0] = 0; itr[0] < 3; ++itr[0])
    {
        bool b = false;
        for(int i = 0; i < 3; ++i)
            if(itr[i] != crd[i] && crd[i] != -1)
            {
                b = true;
                break;
            }
        if(b)
            continue;

        vRet.push_back(at(itr));
    }

    return vRet;
}

vector<int> Crd(int nD, int nV)
{
    vector<int> vCrd;
    for(int i = 0; i < 3; ++i)
        if(i == nD)
            vCrd.push_back(nV);
        else
            vCrd.push_back(-1);
    return vCrd;
}

int* AtFace(vector<int*> vF, int x, int y)
{
    return vF.at(x + y * 3);
}


vector<int*> Cube::GetFace(int nF)
{
    if(nF == 0)
        return GetFace(Crd(0, 0));
    else if(nF == 1)
        return GetFace(Crd(0, 2));
    else if(nF == 2)
        return GetFace(Crd(1, 0));
    else if(nF == 3)
        return GetFace(Crd(1, 2));
    else if(nF == 4)
        return GetFace(Crd(2, 0));
    else if(nF == 5)
        return GetFace(Crd(2, 2));
    throw string("Fool!");
}

vector<int*> FlipH(vector<int*> vF)
{
    vector<int*> vRet;
    for(int y = 0; y < 3; ++y)
    for(int x = 0; x < 3; ++x)
        vRet.push_back(AtFace(vF, 2 - x, y));
    return vRet;
}

vector<int*> FlipV(vector<int*> vF)
{
    vector<int*> vRet;
    for(int y = 0; y < 3; ++y)
    for(int x = 0; x < 3; ++x)
        vRet.push_back(AtFace(vF, x, 2 - y));
    return vRet;
}

vector<int*> Transpose(vector<int*> vF)
{
    vector<int*> vRet;
    for(int y = 0; y < 3; ++y)
    for(int x = 0; x < 3; ++x)
        vRet.push_back(AtFace(vF, y, x));
    return vRet;
}

void Cube::Out(std::ostream& ostr)
{
    vector<int*> f_0 = GetFace(0);
    vector<int*> f_1 = GetFace(1);
    vector<int*> f_2 = GetFace(2);
    vector<int*> f_3 = GetFace(3);
    vector<int*> f_4 = GetFace(4);
    vector<int*> f_5 = GetFace(5);

    f_0 = FlipV(FlipH(Transpose(FlipH(f_0))));
    f_1 = Transpose(f_1);
    f_2 = FlipV(f_2);
    f_5 = FlipV(f_5);

    std::string str = "   ";

    int x, y;
    for(y = 0; y < 3; ++y)
    {
        ostr << str;
        for(x = 0; x < 3; ++x)
            ostr << *AtFace(f_2, x, y) << "";
        ostr << "\n";
    }

    for(y = 0; y < 3; ++y)
    {
        for(x = 0; x < 3; ++x)
            ostr << *AtFace(f_0, x, y) << "";
        for(x = 0; x < 3; ++x)
            ostr << *AtFace(f_4, x, y) << "";
        for(x = 0; x < 3; ++x)
            ostr << *AtFace(f_1, x, y) << "";
        ostr << "\n";
    }

    for(y = 0; y < 3; ++y)
    {
        ostr << str;
        for(x = 0; x < 3; ++x)
            ostr << *AtFace(f_3, x, y) << "";
        ostr << "\n";
    }

    for(y = 0; y < 3; ++y)
    {
        ostr << str;
        for(x = 0; x < 3; ++x)
            ostr << *AtFace(f_5, x, y) << "";
        ostr << "\n";
    }
}

bool CheckWin(vector<int*> vF, int n)
{
    int x, y;
    for(x = 0; x < 3; ++x)
        if(*AtFace(vF, x, 0) == n)
        if(*AtFace(vF, x, 1) == n)
        if(*AtFace(vF, x, 2) == n)
            return true;
    
    for(y = 0; y < 3; ++y)
        if(*AtFace(vF, 0, y) == n)
        if(*AtFace(vF, 1, y) == n)
        if(*AtFace(vF, 2, y) == n)
            return true;

    if(*AtFace(vF, 0, 0) == n)
    if(*AtFace(vF, 1, 1) == n)
    if(*AtFace(vF, 2, 2) == n)
        return true;

    if(*AtFace(vF, 2, 0) == n)
    if(*AtFace(vF, 1, 1) == n)
    if(*AtFace(vF, 0, 2) == n)
        return true;

    return false;
}

bool Cube::CheckWin(int n)
{
    for(int i = 0; i < 6; ++i)
        if(::CheckWin(GetFace(i), n))
            return true;
    return false;
}

int Opp(int nF)
{
    if(nF == 0)
        return 1;
    if(nF == 1)
        return 0;
    if(nF == 2)
        return 3;
    if(nF == 3)
        return 2;
    if(nF == 4)
        return 5;
    if(nF == 5)
        return 4;
    throw string("Fool!");
}

vector<int> vTurns(int nF)
{
    vector<int> vRet;
    for(int i = 0; i < 6; ++i)
        if(i != nF && i != Opp(nF))
            vRet.push_back(i);
    return vRet;
}



